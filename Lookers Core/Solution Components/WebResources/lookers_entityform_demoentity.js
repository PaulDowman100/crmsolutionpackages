var Lookers =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 48);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Guid = /** @class */ (function () {
    function Guid(value) {
        value = value.replace(/[{}]/g, "");
        if (/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/.test(value)) {
            this.value = value.toUpperCase();
        }
        else {
            throw Error("Id " + value + " is not a valid GUID");
        }
    }
    Guid.prototype.areEqual = function (compare) {
        if (this === null || compare === null || this === undefined || compare === undefined) {
            return false;
        }
        return this.value.toLowerCase() === compare.value.toLowerCase();
    };
    return Guid;
}());
exports.Guid = Guid;
var WebApiBase = /** @class */ (function () {
    /**
     * Constructor
     * @param version Version must be 8.0, 8.1 or 8.2
     * @param accessToken Optional access token if using from outside Dynamics 365
     */
    function WebApiBase(version, accessToken, url) {
        this.version = version;
        this.accessToken = accessToken;
        this.url = url;
    }
    /**
     * Get the OData URL
     * @param queryString Query string to append to URL. Defaults to a blank string
     */
    WebApiBase.prototype.getClientUrl = function (queryString) {
        if (queryString === void 0) { queryString = ""; }
        if (this.url != null) {
            return this.url + "/api/data/v" + this.version + "/" + queryString;
        }
        var context = typeof GetGlobalContext !== "undefined" ? GetGlobalContext() : Xrm.Page.context;
        var url = context.getClientUrl() + "/api/data/v" + this.version + "/" + queryString;
        return url;
    };
    /**
     * Retrieve a record from CRM
     * @param entityType Type of entity to retrieve
     * @param id Id of record to retrieve
     * @param queryString OData query string parameters
     * @param queryOptions Various query options for the query
     */
    WebApiBase.prototype.retrieve = function (entitySet, id, queryString, queryOptions) {
        if (queryString != null && !/^[?]/.test(queryString)) {
            queryString = "?" + queryString;
        }
        var query = (queryString != null) ? entitySet + "(" + id.value + ")" + queryString : entitySet + "(" + id.value + ")";
        var req = this.getRequest("GET", query, queryOptions);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 200) {
                        resolve(JSON.parse(req.response));
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            req.send();
        });
    };
    /**
     * Retrieve multiple records from CRM
     * @param entitySet Type of entity to retrieve
     * @param queryString OData query string parameters
     * @param queryOptions Various query options for the query
     */
    WebApiBase.prototype.retrieveMultiple = function (entitySet, queryString, queryOptions) {
        if (queryString != null && !/^[?]/.test(queryString)) {
            queryString = "?" + queryString;
        }
        var query = (queryString != null) ? entitySet + queryString : entitySet;
        var req = this.getRequest("GET", query, queryOptions);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 200) {
                        resolve(JSON.parse(req.response));
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            req.send();
        });
    };
    /**
     * Retrieve next page from a retrieveMultiple request
     * @param query Query from the @odata.nextlink property of a retrieveMultiple
     * @param queryOptions Various query options for the query
     */
    WebApiBase.prototype.getNextPage = function (query, queryOptions) {
        var req = this.getRequest("GET", query, queryOptions, null, false);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 200) {
                        resolve(JSON.parse(req.response));
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            req.send();
        });
    };
    /**
     * Create a record in CRM
     * @param entitySet Type of entity to create
     * @param entity Entity to create
     * @param impersonateUser Impersonate another user
     */
    WebApiBase.prototype.create = function (entitySet, entity, queryOptions) {
        var req = this.getRequest("POST", entitySet, queryOptions);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 204) {
                        var uri = req.getResponseHeader("OData-EntityId");
                        var start = uri.indexOf("(") + 1;
                        var end = uri.indexOf(")", start);
                        var id = uri.substring(start, end);
                        var createdEntity = {
                            id: new Guid(id),
                            uri: uri,
                        };
                        resolve(createdEntity);
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            req.send(JSON.stringify(entity));
        });
    };
    /**
     * Create a record in CRM and return data
     * @param entitySet Type of entity to create
     * @param entity Entity to create
     * @param select Select odata query parameter
     * @param impersonateUser Impersonate another user
     */
    WebApiBase.prototype.createWithReturnData = function (entitySet, entity, select, queryOptions) {
        if (select != null && !/^[?]/.test(select)) {
            select = "?" + select;
        }
        // set reprensetation
        if (queryOptions == null) {
            queryOptions = {};
        }
        queryOptions.representation = true;
        var req = this.getRequest("POST", entitySet + select, queryOptions);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 201) {
                        resolve(JSON.parse(req.response));
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            req.send(JSON.stringify(entity));
        });
    };
    /**
     * Update a record in CRM
     * @param entitySet Type of entity to update
     * @param id Id of record to update
     * @param entity Entity fields to update
     * @param impersonateUser Impersonate another user
     */
    WebApiBase.prototype.update = function (entitySet, id, entity, queryOptions) {
        var req = this.getRequest("PATCH", entitySet + "(" + id.value + ")", queryOptions);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 204) {
                        resolve();
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            req.send(JSON.stringify(entity));
        });
    };
    /**
     * Update a single property of a record in CRM
     * @param entitySet Type of entity to update
     * @param id Id of record to update
     * @param attribute Attribute to update
     * @param impersonateUser Impersonate another user
     */
    WebApiBase.prototype.updateProperty = function (entitySet, id, attribute, value, queryOptions) {
        var req = this.getRequest("PUT", entitySet + "(" + id.value + ")/" + attribute, queryOptions);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 204) {
                        resolve();
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            req.send(JSON.stringify({ value: value }));
        });
    };
    /**
     * Delete a record from CRM
     * @param entitySet Type of entity to delete
     * @param id Id of record to delete
     */
    WebApiBase.prototype.delete = function (entitySet, id) {
        var req = this.getRequest("DELETE", entitySet + "(" + id.value + ")", null);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 204) {
                        resolve();
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            req.send();
        });
    };
    /**
     * Delete a property from a record in CRM. Non navigation properties only
     * @param entitySet Type of entity to update
     * @param id Id of record to update
     * @param attribute Attribute to delete
     */
    WebApiBase.prototype.deleteProperty = function (entitySet, id, attribute) {
        var queryString = "/" + attribute;
        var req = this.getRequest("DELETE", entitySet + "(" + id.value + ")" + queryString, null);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 204) {
                        resolve();
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            req.send();
        });
    };
    /**
     * Associate two records
     * @param entitySet Type of entity for primary record
     * @param id Id of primary record
     * @param relationship Schema name of relationship
     * @param relatedEntitySet Type of entity for secondary record
     * @param relatedEntityId Id of secondary record
     * @param impersonateUser Impersonate another user
     */
    WebApiBase.prototype.associate = function (entitySet, id, relationship, relatedEntitySet, relatedEntityId, queryOptions) {
        var _this = this;
        var req = this.getRequest("POST", entitySet + "(" + id.value + ")/" + relationship + "/$ref", queryOptions);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 204) {
                        resolve();
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            var related = {
                "@odata.id": _this.getClientUrl(relatedEntitySet + "(" + relatedEntityId.value + ")")
            };
            req.send(JSON.stringify(related));
        });
    };
    /**
     * Disassociate two records
     * @param entitySet Type of entity for primary record
     * @param id  Id of primary record
     * @param property Schema name of property or relationship
     * @param relatedEntityId Id of secondary record. Only needed for collection-valued navigation properties
     */
    WebApiBase.prototype.disassociate = function (entitySet, id, property, relatedEntityId) {
        var queryString = property;
        if (relatedEntityId != null) {
            queryString += "(" + relatedEntityId.value + ")";
        }
        queryString += "/$ref";
        var req = this.getRequest("DELETE", entitySet + "(" + id.value + ")/" + queryString, null);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 204) {
                        resolve();
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            req.send();
        });
    };
    /**
     * Execute a default or custom bound action in CRM
     * @param entitySet Type of entity to run the action against
     * @param id Id of record to run the action against
     * @param actionName Name of the action to run
     * @param inputs Any inputs required by the action
     * @param impersonateUser Impersonate another user
     */
    WebApiBase.prototype.boundAction = function (entitySet, id, actionName, inputs, queryOptions) {
        var req = this.getRequest("POST", entitySet + "(" + id.value + ")/Microsoft.Dynamics.CRM." + actionName, queryOptions);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 200) {
                        resolve(JSON.parse(req.response));
                    }
                    else if (req.status === 204) {
                        resolve();
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            inputs != null ? req.send(JSON.stringify(inputs)) : req.send();
        });
    };
    /**
     * Execute a default or custom unbound action in CRM
     * @param actionName Name of the action to run
     * @param inputs Any inputs required by the action
     * @param impersonateUser Impersonate another user
     */
    WebApiBase.prototype.unboundAction = function (actionName, inputs, queryOptions) {
        var req = this.getRequest("POST", actionName, queryOptions);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 200) {
                        resolve(JSON.parse(req.response));
                    }
                    else if (req.status === 204) {
                        resolve();
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            inputs != null ? req.send(JSON.stringify(inputs)) : req.send();
        });
    };
    /**
     * Execute a default or custom bound action in CRM
     * @param entitySet Type of entity to run the action against
     * @param id Id of record to run the action against
     * @param functionName Name of the action to run
     * @param inputs Any inputs required by the action
     * @param impersonateUser Impersonate another user
     */
    WebApiBase.prototype.boundFunction = function (entitySet, id, functionName, inputs, queryOptions) {
        var queryString = entitySet + "(" + id.value + ")/Microsoft.Dynamics.CRM." + functionName + "(";
        queryString = this.getFunctionInputs(queryString, inputs);
        var req = this.getRequest("GET", queryString, queryOptions);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 200) {
                        resolve(JSON.parse(req.response));
                    }
                    else if (req.status === 204) {
                        resolve();
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            inputs != null ? req.send(JSON.stringify(inputs)) : req.send();
        });
    };
    /**
     * Execute an unbound function in CRM
     * @param functionName Name of the action to run
     * @param inputs Any inputs required by the action
     * @param impersonateUser Impersonate another user
     */
    WebApiBase.prototype.unboundFunction = function (functionName, inputs, queryOptions) {
        var queryString = functionName + "(";
        queryString = this.getFunctionInputs(queryString, inputs);
        var req = this.getRequest("GET", queryString, queryOptions);
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 200) {
                        resolve(JSON.parse(req.response));
                    }
                    else if (req.status === 204) {
                        resolve();
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            inputs != null ? req.send(JSON.stringify(inputs)) : req.send();
        });
    };
    /**
     * Execute a batch operation in CRM
     * @param batchId Unique batch id for the operation
     * @param changeSetId Unique change set id for any changesets in the operation
     * @param changeSets Array of change sets (create or update) for the operation
     * @param batchGets Array of get requests for the operation
     * @param impersonateUser Impersonate another user
     */
    WebApiBase.prototype.batchOperation = function (batchId, changeSetId, changeSets, batchGets, queryOptions) {
        var req = this.getRequest("POST", "$batch", queryOptions, "multipart/mixed;boundary=batch_" + batchId);
        // build post body
        var body = [];
        if (changeSets.length > 0) {
            body.push("--batch_" + batchId);
            body.push("Content-Type: multipart/mixed;boundary=changeset_" + changeSetId);
            body.push("");
        }
        // push change sets to body
        for (var i = 0; i < changeSets.length; i++) {
            body.push("--changeset_" + changeSetId);
            body.push("Content-Type: application/http");
            body.push("Content-Transfer-Encoding:binary");
            body.push("Content-ID: " + (i + 1));
            body.push("");
            body.push("POST " + this.getClientUrl(changeSets[i].queryString) + " HTTP/1.1");
            body.push("Content-Type: application/json;type=entry");
            body.push("");
            body.push(JSON.stringify(changeSets[i].entity));
        }
        if (changeSets.length > 0) {
            body.push("--changeset_" + changeSetId + "--");
            body.push("");
        }
        // push get requests to body
        for (var _i = 0, batchGets_1 = batchGets; _i < batchGets_1.length; _i++) {
            var get = batchGets_1[_i];
            body.push("--batch_" + batchId);
            body.push("Content-Type: application/http");
            body.push("Content-Transfer-Encoding:binary");
            body.push("");
            body.push("GET " + this.getClientUrl(get) + " HTTP/1.1");
            body.push("Accept: application/json");
        }
        if (batchGets.length > 0) {
            body.push("");
        }
        body.push("--batch_" + batchId + "--");
        return new Promise(function (resolve, reject) {
            req.onreadystatechange = function () {
                if (req.readyState === 4 /* complete */) {
                    req.onreadystatechange = null;
                    if (req.status === 200) {
                        resolve(req.response);
                    }
                    else if (req.status === 204) {
                        resolve();
                    }
                    else {
                        reject(JSON.parse(req.response).error);
                    }
                }
            };
            req.send(body.join("\r\n"));
        });
    };
    WebApiBase.prototype.getRequest = function (method, queryString, queryOptions, contentType, needsUrl) {
        if (contentType === void 0) { contentType = "application/json; charset=utf-8"; }
        if (needsUrl === void 0) { needsUrl = true; }
        var url;
        if (needsUrl) {
            url = this.getClientUrl(queryString);
        }
        else {
            url = queryString;
        }
        // build XMLHttpRequest
        var request = new XMLHttpRequest();
        request.open(method, url, true);
        request.setRequestHeader("Accept", "application/json");
        request.setRequestHeader("Content-Type", contentType);
        request.setRequestHeader("OData-MaxVersion", "4.0");
        request.setRequestHeader("OData-Version", "4.0");
        request.setRequestHeader("Cache-Control", "no-cache");
        if (queryOptions != null && typeof (queryOptions) !== "undefined") {
            request.setRequestHeader("Prefer", this.getPreferHeader(queryOptions));
            if (queryOptions.impersonateUser != null) {
                request.setRequestHeader("MSCRMCallerID", queryOptions.impersonateUser.value);
            }
        }
        if (this.accessToken != null) {
            request.setRequestHeader("Authorization", "Bearer " + this.accessToken);
        }
        return request;
    };
    WebApiBase.prototype.getPreferHeader = function (queryOptions) {
        var prefer = [];
        // add max page size to prefer request header
        if (queryOptions.maxPageSize) {
            prefer.push("odata.maxpagesize=" + queryOptions.maxPageSize);
        }
        // add formatted values to prefer request header
        if (queryOptions.includeFormattedValues && queryOptions.includeLookupLogicalNames &&
            queryOptions.includeAssociatedNavigationProperties) {
            prefer.push("odata.include-annotations=\"*\"");
        }
        else {
            var preferExtra = [
                queryOptions.includeFormattedValues ? "OData.Community.Display.V1.FormattedValue" : "",
                queryOptions.includeLookupLogicalNames ? "Microsoft.Dynamics.CRM.lookuplogicalname" : "",
                queryOptions.includeAssociatedNavigationProperties ? "Microsoft.Dynamics.CRM.associatednavigationproperty" : "",
            ].filter(function (v, i) {
                return v !== "";
            }).join(",");
            prefer.push("odata.include-annotations=\"" + preferExtra + "\"");
        }
        return prefer.join(",");
    };
    WebApiBase.prototype.getFunctionInputs = function (queryString, inputs) {
        if (inputs == null) {
            return queryString + ")";
        }
        var aliases = "?";
        for (var i = 0; i < inputs.length; i++) {
            queryString += inputs[i].name;
            if (inputs[i].alias) {
                queryString += "=@" + inputs[i].alias + ",";
                aliases += "@" + inputs[i].alias + "=" + inputs[i].value;
            }
            else {
                queryString += "=" + inputs[i].value + ",";
            }
        }
        queryString = queryString.substr(0, queryString.length - 1) + ")";
        if (aliases !== "?") {
            queryString += aliases;
        }
        return queryString;
    };
    return WebApiBase;
}());
exports.WebApiBase = WebApiBase;
var WebApi = /** @class */ (function (_super) {
    __extends(WebApi, _super);
    function WebApi() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return WebApi;
}(WebApiBase));
exports.WebApi = WebApi;


/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var xrm_webapi_1 = __webpack_require__(0);
var WebApiHelper = /** @class */ (function () {
    function WebApiHelper() {
    }
    WebApiHelper.GetWebApiObject = function () {
        return new xrm_webapi_1.WebApi("8.2");
    };
    WebApiHelper.GetLookupName = function (attributeName) {
        // Return the name of a Lookup Attribute in the required format for use in a WebApi query.
        return "_" + attributeName + "_value";
    };
    WebApiHelper.GetLookupId = function (webApiResponse, attributeName) {
        // Get WebApi formatted Attribute name
        var lookupName = WebApiHelper.GetLookupName(attributeName);
        // Return Id
        var id = webApiResponse[lookupName];
        if (!id)
            return null;
        return new xrm_webapi_1.Guid(id);
    };
    WebApiHelper.GetLookupValue = function (webApiResponse, attributeName, knownLogicalName, knownDisplayName) {
        // Get WebApi formatted Attribute name
        var lookupName = WebApiHelper.GetLookupName(attributeName);
        // Get Id
        var id = webApiResponse[lookupName];
        if (!id)
            return null;
        // Get Entity Logical Name from passed-in parameter, or from response
        var logicalName = knownLogicalName;
        if (logicalName == null)
            logicalName = webApiResponse[lookupName + "@Microsoft.Dynamics.CRM.lookuplogicalname"];
        if (logicalName == null) {
            Xrm.Utility.alertDialog("Cannot determine Entity Logical Name. Either supply it explicitly, or query WebApi with the 'Prefer: odata.include-annotations=\"Microsoft.Dynamics.CRM.lookuplogicalname\"' header.", null);
            return null;
        }
        // Get Entity Record Display Name from passed-in parameter, or from response. If
        var displayName = knownDisplayName;
        if (displayName == null)
            displayName = webApiResponse[lookupName + "@OData.Community.Display.V1.FormattedValue"];
        if (displayName == null) {
            Xrm.Utility.alertDialog("Cannot determine Entity Record Display Name. Either supply it explicitly, query WebApi with the 'Prefer: odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"' header, or provide a reference to a webApi object so it can be retrieved.", null);
            return null;
        }
        // Return
        var result = {
            id: id,
            entityType: logicalName,
            name: displayName
        };
        return result;
    };
    WebApiHelper.GetEntityUri = function (entitySetName, id) {
        return Xrm.Page.context.getClientUrl() + "/api/data/v8.2/" + entitySetName + "(" + id.value + ")";
    };
    WebApiHelper.GetEntityMetadata = function (webApi, entityLogicalName, selectList, callback) {
        // Generate unique session key for caching of Entity Metadata
        var fullSessionKey = "em_" + entityLogicalName + selectList + Xrm.Page.context.getUserId() + Xrm.Page.context.getOrgUniqueName();
        // Attempt to retrieve from session storage
        var sessionData = sessionStorage.getItem(fullSessionKey);
        // If retrieved from session storage, pass to callback
        if (sessionData) {
            callback(JSON.parse(sessionData));
        }
        else {
            var queryString = "$select=" + selectList + "&$filter=LogicalName eq '" + entityLogicalName + "'";
            webApi.retrieveMultiple("EntityDefinitions", queryString)
                .then(function (results) {
                sessionStorage.setItem(fullSessionKey, JSON.stringify(results));
                callback(results);
            }, function (error) {
                Xrm.Utility.alertDialog("Failed to retrieve Entity Metadata. [" + error.message + "]", null);
            });
        }
    };
    WebApiHelper.CreateEntityObject = function () {
        // Create an Entity object for the current page
        return { id: new xrm_webapi_1.Guid(Xrm.Page.data.entity.getId()), attributes: {} };
    };
    WebApiHelper.ExecuteWorkflow = function (webApi, workflowId, entityId) {
        // Execute a Workflow.
        webApi.boundAction("workflows", workflowId, "ExecuteWorkflow", { EntityId: entityId }).then(null, function (error) {
            Xrm.Utility.alertDialog("Failed to execute Workflow '" + entityId.toString() + "'.", null);
        });
    };
    return WebApiHelper;
}());
exports.WebApiHelper = WebApiHelper;
//# sourceMappingURL=WebApiHelper.js.map

/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var CrmKeyValuePair_1 = __webpack_require__(7);
var KeyValuePairHelper = /** @class */ (function () {
    function KeyValuePairHelper() {
    }
    KeyValuePairHelper.GetValue = function (webApi, key, errorIfMissing, callback) {
        // Key Value Pairs are retrieved only once per session
        // Try to read value from session
        var sessionKey = "kvp_" + key;
        var kvpValue = sessionStorage[sessionKey];
        // If there is no value, and we don't raise an error, drop out
        if (kvpValue == "[NO VALUE]") {
            if (errorIfMissing)
                kvpValue = null;
            else
                return;
        }
        // If value is present, pass to callback
        if (kvpValue) {
            callback(kvpValue);
        }
        else {
            webApi.retrieveMultiple(CrmKeyValuePair_1.CrmKeyValuePair.EntitySetName, "$filter=" + CrmKeyValuePair_1.CrmKeyValuePair.Key + " eq '" + key + "'&$select=" + CrmKeyValuePair_1.CrmKeyValuePair.Value).then(function (results) {
                var val = results["value"];
                // If KVP not retrieved...
                if (!val || val.length != 1) {
                    if (errorIfMissing) {
                        // ... show error message, or...
                        Xrm.Utility.alertDialog("Could not retrieve Key Value Pair " + key + ".", null);
                        return;
                    }
                    else {
                        // ... store "[NO VALUE]" in session for next time ...
                        sessionStorage[sessionKey] = "[NO VALUE]";
                        return;
                    }
                }
                // Add KVP value to session
                var kvpValue = val[0][CrmKeyValuePair_1.CrmKeyValuePair.Value];
                sessionStorage[sessionKey] = kvpValue;
                // Pass to callback
                callback(kvpValue);
            }, 
            // Display alert if error occurs
            function (error) {
                Xrm.Utility.alertDialog("Could not retrieve Key Value Pair " + key + ". [" + error.message + "]", null);
                return;
            });
        }
    };
    return KeyValuePairHelper;
}());
exports.KeyValuePairHelper = KeyValuePairHelper;
//# sourceMappingURL=KeyValuePairHelper.js.map

/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var xrm_webapi_1 = __webpack_require__(0);
var CrmPhoneCall_1 = __webpack_require__(5);
var CrmEmail_1 = __webpack_require__(4);
var WebApiHelper_1 = __webpack_require__(1);
var AppInsightsHelper_1 = __webpack_require__(6);
var EntityForm = /** @class */ (function () {
    function EntityForm() {
    }
    EntityForm.Initialise = function (context) {
        // Log Page View
        AppInsightsHelper_1.AppInsightsHelper.TrackForm(EntityForm.WebApi);
        // Get Events from parent class
        var events = this.DefineEvents();
        // Associate events with attributes
        if (events != null) {
            for (var i = 0; i < events.length; i++) {
                var ev = events[i];
                var attr = Xrm.Page.getAttribute(ev.Attribute);
                if (attr)
                    attr.addOnChange(ev.Handler);
            }
        }
        // Add common OnSave event
        Xrm.Page.data.entity.addOnSave(this.OnSaveCommon);
        // Add OnSave event which can be overridden in parent class
        Xrm.Page.data.entity.addOnSave(this.OnSave);
        // Call the OnLoad for this form
        this.OnLoad(context);
        // Add Click-to-call and Click-to-email handlers
        parent["Mscrm"].ReadFormUtilities.openPhoneClient = EntityForm.OnClickPhoneNumber;
        parent["Mscrm"].Shortcuts.openEmailForm = EntityForm.OnClickEmailAddress;
    };
    // Overridable methods
    EntityForm.OnLoad = function (context) { };
    EntityForm.OnSave = function (context) { };
    EntityForm.DefineEvents = function () { return null; };
    // Returns null rather than empty string if Id not present
    EntityForm.GetId = function () {
        var id = Xrm.Page.data.entity.getId();
        if (!id || id.length == 0)
            return null;
        return id;
    };
    // This method was implemented to work around a script error bug in CRM when directly copying one lookup value to another.
    // The error occurred in a retrieve to CRM which we don't need to happen, so this method continues to be used
    // even after the fix to the original bug has been applied by Microsoft.
    EntityForm.CopyLookupValue = function (srcVal) {
        return {
            id: srcVal.id,
            name: srcVal.name,
            entityType: srcVal.entityType
        };
    };
    // Common OnSave event
    EntityForm.OnSaveCommon = function () {
        if (Xrm.Page.ui.getFormType() == 1 /* Create */) {
            setTimeout(EntityForm.RefreshWebResources, 3000);
        }
    };
    // Called from Common OnSave event: refresh Web Resources which pass the record ID when a record is initally saved
    EntityForm.RefreshWebResources = function () {
        var id = EntityForm.GetId();
        if (id) {
            // Get Web Resource controls on the page
            var webResourceControls = Xrm.Page.getControl(function (control, index) {
                return control.getControlType() == "webresource";
            });
            // Add the new ID to any which pass parameters
            for (var i = 0; i < webResourceControls.length; i++) {
                var control = webResourceControls[i];
                var url = control.getSrc();
                if (url.indexOf("typename=") > 0) {
                    url += "&id=" + encodeURIComponent(id);
                    control.setSrc(url);
                }
            }
        }
        else {
            setTimeout(EntityForm.RefreshWebResources, 1000);
        }
    };
    EntityForm.GetFormData = function (selectedValue) {
        var formData = {
            Regarding: {
                id: EntityForm.GetId(),
                name: Xrm.Page.data.entity.getPrimaryAttributeValue(),
                entityType: Xrm.Page.data.entity.getEntityName(),
            },
            SelectedValue: selectedValue
        };
        return encodeURIComponent(JSON.stringify(formData));
    };
    EntityForm.OnClickPhoneNumber = function (phoneNumber) {
        // Do nothing if number is clicked on a Phone Call
        var entityType = Xrm.Page.data.entity.getEntityName();
        if (entityType == CrmPhoneCall_1.CrmPhoneCall.EntityLogicalName)
            return;
        // Create data to send to Phone Call form
        var formData = EntityForm.GetFormData(phoneNumber);
        // Find an existing scheduled Phone Call against this record
        EntityForm.WebApi.retrieveMultiple(CrmPhoneCall_1.CrmPhoneCall.EntitySetName, "$select=" + CrmPhoneCall_1.CrmPhoneCall.PrimaryIdAttribute + "&$filter=" + CrmPhoneCall_1.CrmPhoneCall.ActivityStatus + " eq 0 and " +
            WebApiHelper_1.WebApiHelper.GetLookupName(CrmPhoneCall_1.CrmPhoneCall.Regarding) + " eq " + new xrm_webapi_1.Guid(EntityForm.GetId()).value + "&$orderby=" + CrmPhoneCall_1.CrmPhoneCall.Due + " desc").then(function (results) {
            var phoneCalls = results["value"];
            if (phoneCalls && phoneCalls.length > 0) {
                // If a Scheduled Phone Call is found, open it
                Xrm.Utility.openEntityForm(CrmPhoneCall_1.CrmPhoneCall.EntityLogicalName, phoneCalls[0][CrmPhoneCall_1.CrmPhoneCall.PrimaryIdAttribute], { lookers_formdata: formData });
            }
            else {
                // Open a new Phone Call
                Xrm.Utility.openEntityForm(CrmPhoneCall_1.CrmPhoneCall.EntityLogicalName, null, { lookers_formdata: formData });
            }
        }, function (error) {
            Xrm.Utility.alertDialog("Failed to retrieve Phone Calls. [" + error.message + "]", null);
        });
    };
    EntityForm.OnClickEmailAddress = function (emailAddress) {
        // Do nothing if email address is clicked on an Email
        var entityType = Xrm.Page.data.entity.getEntityName();
        if (entityType == CrmEmail_1.CrmEmail.EntityLogicalName)
            return;
        // Create data to send to Email form
        var formData = EntityForm.GetFormData();
        // Find an existing scheduled Email against this record
        EntityForm.WebApi.retrieveMultiple(CrmEmail_1.CrmEmail.EntitySetName, "$select=" + CrmEmail_1.CrmEmail.PrimaryIdAttribute + "&$filter=" + CrmEmail_1.CrmEmail.ActivityStatus + " eq 0 and " +
            WebApiHelper_1.WebApiHelper.GetLookupName(CrmEmail_1.CrmEmail.Regarding) + " eq " + new xrm_webapi_1.Guid(EntityForm.GetId()).value + "&$orderby=" + CrmEmail_1.CrmEmail.DueDate + " desc").then(function (results) {
            var emails = results["value"];
            if (emails && emails.length > 0) {
                // If a Scheduled Email is found, open it
                Xrm.Utility.openEntityForm(CrmEmail_1.CrmEmail.EntityLogicalName, emails[0][CrmPhoneCall_1.CrmPhoneCall.PrimaryIdAttribute], { lookers_formdata: formData });
            }
            else {
                // Open a new Email
                Xrm.Utility.openEntityForm(CrmEmail_1.CrmEmail.EntityLogicalName, null, { lookers_formdata: formData });
            }
        }, function (error) {
            Xrm.Utility.alertDialog("Failed to retrieve Emails. [" + error.message + "]", null);
        });
    };
    EntityForm.LockControls = function (attrName, locked) {
        EntityForm.LockAttributeControls(Xrm.Page.getAttribute(attrName), locked);
    };
    EntityForm.LockAttributeControls = function (attr, locked) {
        if (attr == null)
            return;
        attr.setSubmitMode("dirty");
        attr.controls.forEach(function (c) { c.setDisabled(locked); });
    };
    EntityForm.LockEditableControls = function () {
        var lockedCtrls = Xrm.Page.getControl(function (c) { try {
            return !c.getDisabled();
        }
        catch (ex) {
            return false;
        } });
        lockedCtrls.forEach(function (c) { c.setDisabled(true); });
        return lockedCtrls;
    };
    EntityForm.RestoreEditableControls = function (ctrls) {
        ctrls.forEach(function (c) { c.setDisabled(false); });
    };
    EntityForm.RemoveMandatoryAttributes = function () {
        var requiredAttrs = Xrm.Page.getAttribute(function (a) { return a.getRequiredLevel() == "required"; });
        requiredAttrs.forEach(function (a) { a.setRequiredLevel("none"); });
        return requiredAttrs;
    };
    EntityForm.RestoreMandatoryAttributes = function (attrs, wait) {
        setTimeout(function () { attrs.forEach(function (a) { a.setRequiredLevel("required"); }); }, wait);
    };
    EntityForm.Refresh = function (save) {
        var attrs = EntityForm.RemoveMandatoryAttributes();
        EntityForm.RestoreMandatoryAttributes(attrs, 1000);
        return Xrm.Page.data.refresh(save);
    };
    EntityForm.ForceSave = function () {
        var attrs = EntityForm.RemoveMandatoryAttributes();
        Xrm.Page.data.save().then(function () {
            Xrm.Page.data.refresh(true).then(function () {
                EntityForm.RestoreMandatoryAttributes(attrs, 1000);
            }, null);
        }, null);
    };
    EntityForm.AddGridLoadEvent = function (gridName, handler) {
        try {
            // Add on-load event to Stock Grid
            var gridControl = Xrm.Page.getControl(gridName);
            gridControl.addOnLoad(function () { setTimeout(handler, 1500, gridControl); });
            // Call on-load event now
            setTimeout(handler, 3000, gridControl);
        }
        catch (ex) {
            // An exception can occur when this script runs and the page is loading. The Grid is not ready to be accessed. Call method again in 1 second.
            setTimeout(EntityForm.AddGridLoadEvent, 1000, gridName, handler);
        }
    };
    EntityForm.SwitchToForm = function (formLabel) {
        var formSelector = Xrm.Page.ui.formSelector;
        if (formSelector.getCurrentItem().getLabel() == formLabel)
            return;
        formSelector.items.forEach(function (f) { if (f.getLabel() == formLabel)
            f.navigate(); });
    };
    EntityForm.WebApi = WebApiHelper_1.WebApiHelper.GetWebApiObject();
    EntityForm.QOAllInclude = {
        includeAssociatedNavigationProperties: true,
        includeFormattedValues: true,
        includeLookupLogicalNames: true
    };
    return EntityForm;
}());
exports.EntityForm = EntityForm;
;
var ActivityFormData = /** @class */ (function () {
    function ActivityFormData() {
    }
    return ActivityFormData;
}());
exports.ActivityFormData = ActivityFormData;
var EventDefinition = /** @class */ (function () {
    function EventDefinition(attribute, handler) {
        this.Attribute = attribute;
        this.Handler = handler;
    }
    return EventDefinition;
}());
exports.EventDefinition = EventDefinition;
//# sourceMappingURL=EntityForm.js.map

/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var CrmEmail = /** @class */ (function () {
    function CrmEmail() {
    }
    CrmEmail.EntityLogicalName = "email";
    CrmEmail.EntitySetName = "emails";
    CrmEmail.PrimaryIdAttribute = "activityid";
    CrmEmail.StartDate = "scheduledstart";
    CrmEmail.StatusReason = "statuscode";
    CrmEmail.SubmittedBy = "submittedby";
    CrmEmail.Description = "description";
    CrmEmail.To = "to";
    CrmEmail.IsWorkflowCreated = "isworkflowcreated";
    CrmEmail.DueDate = "scheduledend";
    CrmEmail.MimeType = "mimetype";
    CrmEmail.CreatedBy = "createdby";
    CrmEmail.VersionNumber = "versionnumber";
    CrmEmail.ReadReceiptRequested = "readreceiptrequested";
    CrmEmail.SubCategory = "subcategory";
    CrmEmail.IsBilled = "isbilled";
    CrmEmail.Duration = "actualdurationminutes";
    CrmEmail.Priority = "prioritycode";
    CrmEmail.ModifiedBy = "modifiedby";
    CrmEmail.ActualStart = "actualstart";
    CrmEmail.From = "from";
    CrmEmail.Direction = "directioncode";
    CrmEmail.ActualEnd = "actualend";
    CrmEmail.TrackingToken = "trackingtoken";
    CrmEmail.Service = "serviceid";
    CrmEmail.ScheduledDuration = "scheduleddurationminutes";
    CrmEmail.Category = "category";
    CrmEmail.CreatedOn = "createdon";
    CrmEmail.OwningBusinessUnit = "owningbusinessunit";
    CrmEmail.Bcc = "bcc";
    CrmEmail.Cc = "cc";
    CrmEmail.From1 = "sender";
    CrmEmail.Subject = "subject";
    CrmEmail.ModifiedOn = "modifiedon";
    CrmEmail.ToRecipients = "torecipients";
    CrmEmail.DeliveryReceiptRequested = "deliveryreceiptrequested";
    CrmEmail.Regarding = "regardingobjectid";
    CrmEmail.ActivityStatus = "statecode";
    CrmEmail.Owner = "ownerid";
    CrmEmail.MessageID = "messageid";
    CrmEmail.OwningUser = "owninguser";
    CrmEmail.RecordCreatedOn = "overriddencreatedon";
    CrmEmail.ImportSequenceNumber = "importsequencenumber";
    CrmEmail.NoofDeliveryAttempts = "deliveryattempts";
    CrmEmail.Compression = "compressed";
    CrmEmail.Notifications = "notifications";
    CrmEmail.TimeZoneRuleVersionNumber = "timezoneruleversionnumber";
    CrmEmail.UTCConversionTimeZoneCode = "utcconversiontimezonecode";
    CrmEmail.CreatedByDelegate = "createdonbehalfby";
    CrmEmail.ModifiedByDelegate = "modifiedonbehalfby";
    CrmEmail.ActivityType = "activitytypecode";
    CrmEmail.IsRegularActivity = "isregularactivity";
    CrmEmail.OwningTeam = "owningteam";
    CrmEmail.Currency = "transactioncurrencyid";
    CrmEmail.ExchangeRate = "exchangerate";
    CrmEmail.Sender = "emailsender";
    CrmEmail.EmailSenderName = "emailsendername";
    CrmEmail.EmailSenderType = "emailsenderobjecttypecode";
    CrmEmail.SendersAccount = "sendersaccount";
    CrmEmail.SendersAccountType = "sendersaccountobjecttypecode";
    CrmEmail.AttachmentCount = "attachmentcount";
    CrmEmail.SendersMailbox = "sendermailboxid";
    CrmEmail.DeliveryPriority = "deliveryprioritycode";
    CrmEmail.ParentActivityId = "parentactivityid";
    CrmEmail.ParentActivityName = "parentactivityidname";
    CrmEmail.InReplyToMessage = "inreplyto";
    CrmEmail.ConversationIndexHash = "baseconversationindexhash";
    CrmEmail.ConversationIndex = "conversationindex";
    CrmEmail.CorrelationMethod = "correlationmethod";
    CrmEmail.DateSent = "senton";
    CrmEmail.Delayemailprocessinguntil = "postponeemailprocessinguntil";
    CrmEmail.SafeDescription = "safedescription";
    CrmEmail.Process = "processid";
    CrmEmail.ProcessStage = "stageid";
    CrmEmail.AdditionalParameters = "activityadditionalparams";
    CrmEmail.IsUnsafe = "isunsafe";
    CrmEmail.SLA = "slaid";
    CrmEmail.LastSLAapplied = "slainvokedid";
    CrmEmail.OnHoldTimeMinutes = "onholdtime";
    CrmEmail.LastOnHoldTime = "lastonholdtime";
    CrmEmail.EmailSenderAccountName = "sendersaccountname";
    CrmEmail.EmailSenderAccountyomiName = "sendersaccountyominame";
    CrmEmail.EmailSenderyomiName = "emailsenderyominame";
    CrmEmail.TraversedPath = "traversedpath";
    CrmEmail.AttachmentViewCount = "attachmentopencount";
    CrmEmail.ConversationTrackingId = "conversationtrackingid";
    CrmEmail.DelaySend = "delayedemailsendtime";
    CrmEmail.LastOpenedTime = "lastopenedtime";
    CrmEmail.LinkClickCount = "linksclickedcount";
    CrmEmail.OpenCount = "opencount";
    CrmEmail.ReplyCount = "replycount";
    CrmEmail.EmailTrackingId = "emailtrackingid";
    CrmEmail.Following = "followemailuserpreference";
    CrmEmail.Followed = "isemailfollowed";
    CrmEmail.EmailReminderExpiryTime = "emailreminderexpirytime";
    CrmEmail.EmailReminderType = "emailremindertype";
    CrmEmail.EmailReminderStatus = "emailreminderstatus";
    CrmEmail.EmailReminderText = "emailremindertext";
    CrmEmail.IDfortemplateused = "templateid";
    CrmEmail.ReminderActionCardId = "reminderactioncardid";
    CrmEmail.SortDate = "sortdate";
    CrmEmail.AttachmentCount1 = "lookers_attachmentcount";
    CrmEmail.FastrackActivityType = "lookers_fastrackactivitytype";
    return CrmEmail;
}());
exports.CrmEmail = CrmEmail;
var CrmEmail_StatusReasonOptions;
(function (CrmEmail_StatusReasonOptions) {
    CrmEmail_StatusReasonOptions[CrmEmail_StatusReasonOptions["Draft"] = 1] = "Draft";
    CrmEmail_StatusReasonOptions[CrmEmail_StatusReasonOptions["Completed"] = 2] = "Completed";
    CrmEmail_StatusReasonOptions[CrmEmail_StatusReasonOptions["Sent"] = 3] = "Sent";
    CrmEmail_StatusReasonOptions[CrmEmail_StatusReasonOptions["Received"] = 4] = "Received";
    CrmEmail_StatusReasonOptions[CrmEmail_StatusReasonOptions["Canceled"] = 5] = "Canceled";
    CrmEmail_StatusReasonOptions[CrmEmail_StatusReasonOptions["PendingSend"] = 6] = "PendingSend";
    CrmEmail_StatusReasonOptions[CrmEmail_StatusReasonOptions["Sending"] = 7] = "Sending";
    CrmEmail_StatusReasonOptions[CrmEmail_StatusReasonOptions["Failed"] = 8] = "Failed";
})(CrmEmail_StatusReasonOptions = exports.CrmEmail_StatusReasonOptions || (exports.CrmEmail_StatusReasonOptions = {}));
var CrmEmail_PriorityOptions;
(function (CrmEmail_PriorityOptions) {
    CrmEmail_PriorityOptions[CrmEmail_PriorityOptions["Low"] = 0] = "Low";
    CrmEmail_PriorityOptions[CrmEmail_PriorityOptions["Normal"] = 1] = "Normal";
    CrmEmail_PriorityOptions[CrmEmail_PriorityOptions["High"] = 2] = "High";
})(CrmEmail_PriorityOptions = exports.CrmEmail_PriorityOptions || (exports.CrmEmail_PriorityOptions = {}));
var CrmEmail_StatusOptions;
(function (CrmEmail_StatusOptions) {
    CrmEmail_StatusOptions[CrmEmail_StatusOptions["Open"] = 0] = "Open";
    CrmEmail_StatusOptions[CrmEmail_StatusOptions["Completed"] = 1] = "Completed";
    CrmEmail_StatusOptions[CrmEmail_StatusOptions["Canceled"] = 2] = "Canceled";
})(CrmEmail_StatusOptions = exports.CrmEmail_StatusOptions || (exports.CrmEmail_StatusOptions = {}));
var CrmEmail_NotificationsOptions;
(function (CrmEmail_NotificationsOptions) {
    CrmEmail_NotificationsOptions[CrmEmail_NotificationsOptions["None"] = 0] = "None";
    CrmEmail_NotificationsOptions[CrmEmail_NotificationsOptions["ThemessagewassavedasaMicrosoftDynamics365emailrecordbutnotalltheattachmentscouldbesavedwithitAnattachmentcannotbesavedifitisblockedorifitsfiletypeisinvalid"] = 1] = "ThemessagewassavedasaMicrosoftDynamics365emailrecordbutnotalltheattachmentscouldbesavedwithitAnattachmentcannotbesavedifitisblockedorifitsfiletypeisinvalid";
    CrmEmail_NotificationsOptions[CrmEmail_NotificationsOptions["Truncatedbody"] = 2] = "Truncatedbody";
})(CrmEmail_NotificationsOptions = exports.CrmEmail_NotificationsOptions || (exports.CrmEmail_NotificationsOptions = {}));
var CrmEmail_DeliveryPriorityOptions;
(function (CrmEmail_DeliveryPriorityOptions) {
    CrmEmail_DeliveryPriorityOptions[CrmEmail_DeliveryPriorityOptions["Low"] = 0] = "Low";
    CrmEmail_DeliveryPriorityOptions[CrmEmail_DeliveryPriorityOptions["Normal"] = 1] = "Normal";
    CrmEmail_DeliveryPriorityOptions[CrmEmail_DeliveryPriorityOptions["High"] = 2] = "High";
})(CrmEmail_DeliveryPriorityOptions = exports.CrmEmail_DeliveryPriorityOptions || (exports.CrmEmail_DeliveryPriorityOptions = {}));
var CrmEmail_CorrelationMethodOptions;
(function (CrmEmail_CorrelationMethodOptions) {
    CrmEmail_CorrelationMethodOptions[CrmEmail_CorrelationMethodOptions["None"] = 0] = "None";
    CrmEmail_CorrelationMethodOptions[CrmEmail_CorrelationMethodOptions["Skipped"] = 1] = "Skipped";
    CrmEmail_CorrelationMethodOptions[CrmEmail_CorrelationMethodOptions["XHeader"] = 2] = "XHeader";
    CrmEmail_CorrelationMethodOptions[CrmEmail_CorrelationMethodOptions["InReplyTo"] = 3] = "InReplyTo";
    CrmEmail_CorrelationMethodOptions[CrmEmail_CorrelationMethodOptions["TrackingToken"] = 4] = "TrackingToken";
    CrmEmail_CorrelationMethodOptions[CrmEmail_CorrelationMethodOptions["ConversationIndex"] = 5] = "ConversationIndex";
    CrmEmail_CorrelationMethodOptions[CrmEmail_CorrelationMethodOptions["SmartMatching"] = 6] = "SmartMatching";
    CrmEmail_CorrelationMethodOptions[CrmEmail_CorrelationMethodOptions["CustomCorrelation"] = 7] = "CustomCorrelation";
})(CrmEmail_CorrelationMethodOptions = exports.CrmEmail_CorrelationMethodOptions || (exports.CrmEmail_CorrelationMethodOptions = {}));
var CrmEmail_EmailReminderTypeOptions;
(function (CrmEmail_EmailReminderTypeOptions) {
    CrmEmail_EmailReminderTypeOptions[CrmEmail_EmailReminderTypeOptions["IfIdonotreceiveareplyby"] = 0] = "IfIdonotreceiveareplyby";
    CrmEmail_EmailReminderTypeOptions[CrmEmail_EmailReminderTypeOptions["Iftheemailisnotopenedby"] = 1] = "Iftheemailisnotopenedby";
    CrmEmail_EmailReminderTypeOptions[CrmEmail_EmailReminderTypeOptions["Remindmeanywayat"] = 2] = "Remindmeanywayat";
})(CrmEmail_EmailReminderTypeOptions = exports.CrmEmail_EmailReminderTypeOptions || (exports.CrmEmail_EmailReminderTypeOptions = {}));
var CrmEmail_EmailReminderStatusOptions;
(function (CrmEmail_EmailReminderStatusOptions) {
    CrmEmail_EmailReminderStatusOptions[CrmEmail_EmailReminderStatusOptions["NotSet"] = 0] = "NotSet";
    CrmEmail_EmailReminderStatusOptions[CrmEmail_EmailReminderStatusOptions["ReminderSet"] = 1] = "ReminderSet";
    CrmEmail_EmailReminderStatusOptions[CrmEmail_EmailReminderStatusOptions["ReminderExpired"] = 2] = "ReminderExpired";
    CrmEmail_EmailReminderStatusOptions[CrmEmail_EmailReminderStatusOptions["ReminderInvalid"] = 3] = "ReminderInvalid";
})(CrmEmail_EmailReminderStatusOptions = exports.CrmEmail_EmailReminderStatusOptions || (exports.CrmEmail_EmailReminderStatusOptions = {}));
var CrmEmail_FastrackActivityTypeOptions;
(function (CrmEmail_FastrackActivityTypeOptions) {
    CrmEmail_FastrackActivityTypeOptions[CrmEmail_FastrackActivityTypeOptions["ColdCall"] = 353590000] = "ColdCall";
    CrmEmail_FastrackActivityTypeOptions[CrmEmail_FastrackActivityTypeOptions["Email"] = 353590001] = "Email";
    CrmEmail_FastrackActivityTypeOptions[CrmEmail_FastrackActivityTypeOptions["FollowUp"] = 353590002] = "FollowUp";
    CrmEmail_FastrackActivityTypeOptions[CrmEmail_FastrackActivityTypeOptions["Telephone"] = 353590003] = "Telephone";
    CrmEmail_FastrackActivityTypeOptions[CrmEmail_FastrackActivityTypeOptions["TestDrive"] = 353590004] = "TestDrive";
    CrmEmail_FastrackActivityTypeOptions[CrmEmail_FastrackActivityTypeOptions["Visit"] = 353590005] = "Visit";
})(CrmEmail_FastrackActivityTypeOptions = exports.CrmEmail_FastrackActivityTypeOptions || (exports.CrmEmail_FastrackActivityTypeOptions = {}));
//# sourceMappingURL=CrmEmail.js.map

/***/ }),

/***/ 48:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var EntityForm_1 = __webpack_require__(3);
var CrmDemoEntity_1 = __webpack_require__(49);
var DemoEntityForm = /** @class */ (function (_super) {
    __extends(DemoEntityForm, _super);
    function DemoEntityForm() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    DemoEntityForm.DefineEvents = function () {
        return [
            new EntityForm_1.EventDefinition(CrmDemoEntity_1.CrmDemoEntity.LargeTextField, this.DoSomethingExciting)
        ];
    };
    DemoEntityForm.OnLoad = function (context) {
        Xrm.Page.ui.setFormNotification("The form has loaded.", "INFO", "DEMO");
    };
    DemoEntityForm.OnSave = function (context) {
        Xrm.Utility.alertDialog("You clicked save.", null);
    };
    DemoEntityForm.DoSomethingExciting = function () {
        var attr = Xrm.Page.getAttribute(CrmDemoEntity_1.CrmDemoEntity.LargeTextField);
        Xrm.Utility.alertDialog("You typed: " + attr.getValue(), null);
    };
    return DemoEntityForm;
}(EntityForm_1.EntityForm));
exports.DemoEntityForm = DemoEntityForm;
//# sourceMappingURL=DemoEntity.js.map

/***/ }),

/***/ 49:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var CrmDemoEntity = /** @class */ (function () {
    function CrmDemoEntity() {
    }
    CrmDemoEntity.EntityLogicalName = "lookers_demoentity";
    CrmDemoEntity.EntitySetName = "lookers_demoentities";
    CrmDemoEntity.PrimaryIdAttribute = "lookers_demoentityid";
    CrmDemoEntity.CreatedOn = "createdon";
    CrmDemoEntity.CreatedBy = "createdby";
    CrmDemoEntity.ModifiedOn = "modifiedon";
    CrmDemoEntity.ModifiedBy = "modifiedby";
    CrmDemoEntity.CreatedByDelegate = "createdonbehalfby";
    CrmDemoEntity.ModifiedByDelegate = "modifiedonbehalfby";
    CrmDemoEntity.OrganizationId = "organizationid";
    CrmDemoEntity.Status = "statecode";
    CrmDemoEntity.StatusReason = "statuscode";
    CrmDemoEntity.VersionNumber = "versionnumber";
    CrmDemoEntity.ImportSequenceNumber = "importsequencenumber";
    CrmDemoEntity.RecordCreatedOn = "overriddencreatedon";
    CrmDemoEntity.TimeZoneRuleVersionNumber = "timezoneruleversionnumber";
    CrmDemoEntity.UTCConversionTimeZoneCode = "utcconversiontimezonecode";
    CrmDemoEntity.Name = "lookers_name";
    CrmDemoEntity.ImportantDate = "lookers_importantdate";
    CrmDemoEntity.ExampleDropdown = "lookers_exampledropdown";
    CrmDemoEntity.LargeTextField = "lookers_largetextfield";
    CrmDemoEntity.Customer = "lookers_customerid";
    return CrmDemoEntity;
}());
exports.CrmDemoEntity = CrmDemoEntity;
var CrmDemoEntity_StatusOptions;
(function (CrmDemoEntity_StatusOptions) {
    CrmDemoEntity_StatusOptions[CrmDemoEntity_StatusOptions["Active"] = 0] = "Active";
    CrmDemoEntity_StatusOptions[CrmDemoEntity_StatusOptions["Inactive"] = 1] = "Inactive";
})(CrmDemoEntity_StatusOptions = exports.CrmDemoEntity_StatusOptions || (exports.CrmDemoEntity_StatusOptions = {}));
var CrmDemoEntity_StatusReasonOptions;
(function (CrmDemoEntity_StatusReasonOptions) {
    CrmDemoEntity_StatusReasonOptions[CrmDemoEntity_StatusReasonOptions["Active"] = 1] = "Active";
    CrmDemoEntity_StatusReasonOptions[CrmDemoEntity_StatusReasonOptions["Inactive"] = 2] = "Inactive";
})(CrmDemoEntity_StatusReasonOptions = exports.CrmDemoEntity_StatusReasonOptions || (exports.CrmDemoEntity_StatusReasonOptions = {}));
var CrmDemoEntity_ExampleDropdownOptions;
(function (CrmDemoEntity_ExampleDropdownOptions) {
    CrmDemoEntity_ExampleDropdownOptions[CrmDemoEntity_ExampleDropdownOptions["Low"] = 353590000] = "Low";
    CrmDemoEntity_ExampleDropdownOptions[CrmDemoEntity_ExampleDropdownOptions["Medium"] = 353590001] = "Medium";
    CrmDemoEntity_ExampleDropdownOptions[CrmDemoEntity_ExampleDropdownOptions["High"] = 353590002] = "High";
})(CrmDemoEntity_ExampleDropdownOptions = exports.CrmDemoEntity_ExampleDropdownOptions || (exports.CrmDemoEntity_ExampleDropdownOptions = {}));
//# sourceMappingURL=CrmDemoEntity.js.map

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var CrmPhoneCall = /** @class */ (function () {
    function CrmPhoneCall() {
    }
    CrmPhoneCall.EntityLogicalName = "phonecall";
    CrmPhoneCall.EntitySetName = "phonecalls";
    CrmPhoneCall.PrimaryIdAttribute = "activityid";
    CrmPhoneCall.Subject = "subject";
    CrmPhoneCall.ModifiedBy = "modifiedby";
    CrmPhoneCall.CallTo = "to";
    CrmPhoneCall.OwningBusinessUnit = "owningbusinessunit";
    CrmPhoneCall.ActualEnd = "actualend";
    CrmPhoneCall.CreatedBy = "createdby";
    CrmPhoneCall.PhoneNumber = "phonenumber";
    CrmPhoneCall.Direction = "directioncode";
    CrmPhoneCall.Regarding = "regardingobjectid";
    CrmPhoneCall.IsBilled = "isbilled";
    CrmPhoneCall.ModifiedOn = "modifiedon";
    CrmPhoneCall.Due = "scheduledend";
    CrmPhoneCall.CreatedOn = "createdon";
    CrmPhoneCall.VersionNumber = "versionnumber";
    CrmPhoneCall.Owner = "ownerid";
    CrmPhoneCall.IsWorkflowCreated = "isworkflowcreated";
    CrmPhoneCall.Priority = "prioritycode";
    CrmPhoneCall.ActualStart = "actualstart";
    CrmPhoneCall.Category = "category";
    CrmPhoneCall.ScheduledDuration = "scheduleddurationminutes";
    CrmPhoneCall.CallFrom = "from";
    CrmPhoneCall.Description = "description";
    CrmPhoneCall.ActivityStatus = "statecode";
    CrmPhoneCall.SubCategory = "subcategory";
    CrmPhoneCall.Duration = "actualdurationminutes";
    CrmPhoneCall.StatusReason = "statuscode";
    CrmPhoneCall.StartDate = "scheduledstart";
    CrmPhoneCall.Service = "serviceid";
    CrmPhoneCall.OwningUser = "owninguser";
    CrmPhoneCall.ImportSequenceNumber = "importsequencenumber";
    CrmPhoneCall.RecordCreatedOn = "overriddencreatedon";
    CrmPhoneCall.TimeZoneRuleVersionNumber = "timezoneruleversionnumber";
    CrmPhoneCall.UTCConversionTimeZoneCode = "utcconversiontimezonecode";
    CrmPhoneCall.CreatedByDelegate = "createdonbehalfby";
    CrmPhoneCall.ModifiedByDelegate = "modifiedonbehalfby";
    CrmPhoneCall.ActivityType = "activitytypecode";
    CrmPhoneCall.IsRegularActivity = "isregularactivity";
    CrmPhoneCall.OwningTeam = "owningteam";
    CrmPhoneCall.Currency = "transactioncurrencyid";
    CrmPhoneCall.ExchangeRate = "exchangerate";
    CrmPhoneCall.LeftVoiceMail = "leftvoicemail";
    CrmPhoneCall.Process = "processid";
    CrmPhoneCall.ProcessStage = "stageid";
    CrmPhoneCall.TraversedPath = "traversedpath";
    CrmPhoneCall.AdditionalParameters = "activityadditionalparams";
    CrmPhoneCall.LastSLAapplied = "slainvokedid";
    CrmPhoneCall.OnHoldTimeMinutes = "onholdtime";
    CrmPhoneCall.LastOnHoldTime = "lastonholdtime";
    CrmPhoneCall.SLA = "slaid";
    CrmPhoneCall.SortDate = "sortdate";
    CrmPhoneCall.NewPhoneCall = "lookers_newphonecall";
    CrmPhoneCall.NewPhonecallID = "lookers_newphonecallid";
    CrmPhoneCall.Outcomes = "lookers_outcomes";
    CrmPhoneCall.FastrackActivityType = "lookers_fastrackactivitytype";
    return CrmPhoneCall;
}());
exports.CrmPhoneCall = CrmPhoneCall;
var CrmPhoneCall_PriorityOptions;
(function (CrmPhoneCall_PriorityOptions) {
    CrmPhoneCall_PriorityOptions[CrmPhoneCall_PriorityOptions["Low"] = 0] = "Low";
    CrmPhoneCall_PriorityOptions[CrmPhoneCall_PriorityOptions["Normal"] = 1] = "Normal";
    CrmPhoneCall_PriorityOptions[CrmPhoneCall_PriorityOptions["High"] = 2] = "High";
})(CrmPhoneCall_PriorityOptions = exports.CrmPhoneCall_PriorityOptions || (exports.CrmPhoneCall_PriorityOptions = {}));
var CrmPhoneCall_StatusOptions;
(function (CrmPhoneCall_StatusOptions) {
    CrmPhoneCall_StatusOptions[CrmPhoneCall_StatusOptions["Open"] = 0] = "Open";
    CrmPhoneCall_StatusOptions[CrmPhoneCall_StatusOptions["Completed"] = 1] = "Completed";
    CrmPhoneCall_StatusOptions[CrmPhoneCall_StatusOptions["Canceled"] = 2] = "Canceled";
})(CrmPhoneCall_StatusOptions = exports.CrmPhoneCall_StatusOptions || (exports.CrmPhoneCall_StatusOptions = {}));
var CrmPhoneCall_StatusReasonOptions;
(function (CrmPhoneCall_StatusReasonOptions) {
    CrmPhoneCall_StatusReasonOptions[CrmPhoneCall_StatusReasonOptions["Open"] = 1] = "Open";
    CrmPhoneCall_StatusReasonOptions[CrmPhoneCall_StatusReasonOptions["Successful"] = 2] = "Successful";
    CrmPhoneCall_StatusReasonOptions[CrmPhoneCall_StatusReasonOptions["Unsuccessful"] = 4] = "Unsuccessful";
    CrmPhoneCall_StatusReasonOptions[CrmPhoneCall_StatusReasonOptions["Canceled"] = 3] = "Canceled";
})(CrmPhoneCall_StatusReasonOptions = exports.CrmPhoneCall_StatusReasonOptions || (exports.CrmPhoneCall_StatusReasonOptions = {}));
var CrmPhoneCall_FastrackActivityTypeOptions;
(function (CrmPhoneCall_FastrackActivityTypeOptions) {
    CrmPhoneCall_FastrackActivityTypeOptions[CrmPhoneCall_FastrackActivityTypeOptions["ColdCall"] = 353590000] = "ColdCall";
    CrmPhoneCall_FastrackActivityTypeOptions[CrmPhoneCall_FastrackActivityTypeOptions["Email"] = 353590001] = "Email";
    CrmPhoneCall_FastrackActivityTypeOptions[CrmPhoneCall_FastrackActivityTypeOptions["FollowUp"] = 353590002] = "FollowUp";
    CrmPhoneCall_FastrackActivityTypeOptions[CrmPhoneCall_FastrackActivityTypeOptions["Telephone"] = 353590003] = "Telephone";
    CrmPhoneCall_FastrackActivityTypeOptions[CrmPhoneCall_FastrackActivityTypeOptions["TestDrive"] = 353590004] = "TestDrive";
    CrmPhoneCall_FastrackActivityTypeOptions[CrmPhoneCall_FastrackActivityTypeOptions["Visit"] = 353590005] = "Visit";
})(CrmPhoneCall_FastrackActivityTypeOptions = exports.CrmPhoneCall_FastrackActivityTypeOptions || (exports.CrmPhoneCall_FastrackActivityTypeOptions = {}));
//# sourceMappingURL=CrmPhoneCall.js.map

/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var KeyValuePairHelper_1 = __webpack_require__(2);
var AppInsightsHelper = /** @class */ (function () {
    function AppInsightsHelper() {
    }
    AppInsightsHelper.TrackForm = function (webApi) {
        AppInsightsHelper.Track(webApi, Xrm.Page.data.entity.getEntityName() + "_form");
    };
    AppInsightsHelper.Track = function (webApi, pageId) {
        KeyValuePairHelper_1.KeyValuePairHelper.GetValue(webApi, "Application Insights Instrumentation Key", false, function (instKey) {
            if (instKey) {
                var appInsights = window["appInsights"] || function (config) {
                    function i(config) {
                        t[config] = function () {
                            var i = arguments;
                            t["queue"].push(function () {
                                t[config].apply(t, i);
                            });
                        };
                    }
                    var t = { config: config }, u = document, e = window, o = "script", s = "AuthenticatedUserContext", h = "start", c = "stop", l = "Track", a = l + "Event", v = l + "Page", y = u.createElement(o), r, f;
                    y["src"] = config["url"] || "https://az416426.vo.msecnd.net/scripts/a/ai.0.js";
                    u.getElementsByTagName(o)[0].parentNode.appendChild(y);
                    try {
                        t["cookie"] = u.cookie;
                    }
                    catch (p) { }
                    for (t["queue"] = [],
                        t["version"] = "1.0",
                        r = ["Event", "Exception", "Metric", "PageView", "Trace", "Dependency"]; r.length;)
                        i("track" + r.pop());
                    return i("set" + s),
                        i("clear" + s), i(h + a), i(c + a), i(h + v), i(c + v), i("flush"),
                        config["disableExceptionTracking"] || (r = "onerror", i("_" + r), f = e[r], e[r] = function (config, i, u, e, o) {
                            var s = f && f(config, i, u, e, o);
                            return s !== !0 && t["_" + r](config, i, u, e, o), s;
                        }), t;
                }({
                    instrumentationKey: instKey
                });
                window["appInsights"] = appInsights;
                window["appInsights"].trackPageView(pageId);
            }
        });
    };
    return AppInsightsHelper;
}());
exports.AppInsightsHelper = AppInsightsHelper;
//# sourceMappingURL=AppInsightsHelper.js.map

/***/ }),

/***/ 7:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var CrmKeyValuePair = /** @class */ (function () {
    function CrmKeyValuePair() {
    }
    CrmKeyValuePair.EntityLogicalName = "lookers_keyvaluepair";
    CrmKeyValuePair.EntitySetName = "lookers_keyvaluepairs";
    CrmKeyValuePair.PrimaryIdAttribute = "lookers_keyvaluepairid";
    CrmKeyValuePair.CreatedOn = "createdon";
    CrmKeyValuePair.CreatedBy = "createdby";
    CrmKeyValuePair.ModifiedOn = "modifiedon";
    CrmKeyValuePair.ModifiedBy = "modifiedby";
    CrmKeyValuePair.CreatedByDelegate = "createdonbehalfby";
    CrmKeyValuePair.ModifiedByDelegate = "modifiedonbehalfby";
    CrmKeyValuePair.OrganizationId = "organizationid";
    CrmKeyValuePair.Status = "statecode";
    CrmKeyValuePair.StatusReason = "statuscode";
    CrmKeyValuePair.ImportSequenceNumber = "importsequencenumber";
    CrmKeyValuePair.RecordCreatedOn = "overriddencreatedon";
    CrmKeyValuePair.TimeZoneRuleVersionNumber = "timezoneruleversionnumber";
    CrmKeyValuePair.UTCConversionTimeZoneCode = "utcconversiontimezonecode";
    CrmKeyValuePair.Key = "lookers_key";
    CrmKeyValuePair.Value = "lookers_value";
    return CrmKeyValuePair;
}());
exports.CrmKeyValuePair = CrmKeyValuePair;
var CrmKeyValuePair_StatusOptions;
(function (CrmKeyValuePair_StatusOptions) {
    CrmKeyValuePair_StatusOptions[CrmKeyValuePair_StatusOptions["Active"] = 0] = "Active";
    CrmKeyValuePair_StatusOptions[CrmKeyValuePair_StatusOptions["Inactive"] = 1] = "Inactive";
})(CrmKeyValuePair_StatusOptions = exports.CrmKeyValuePair_StatusOptions || (exports.CrmKeyValuePair_StatusOptions = {}));
var CrmKeyValuePair_StatusReasonOptions;
(function (CrmKeyValuePair_StatusReasonOptions) {
    CrmKeyValuePair_StatusReasonOptions[CrmKeyValuePair_StatusReasonOptions["Active"] = 1] = "Active";
    CrmKeyValuePair_StatusReasonOptions[CrmKeyValuePair_StatusReasonOptions["Inactive"] = 2] = "Inactive";
})(CrmKeyValuePair_StatusReasonOptions = exports.CrmKeyValuePair_StatusReasonOptions || (exports.CrmKeyValuePair_StatusReasonOptions = {}));
//# sourceMappingURL=CrmKeyValuePair.js.map

/***/ })

/******/ });