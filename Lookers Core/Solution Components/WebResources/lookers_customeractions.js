function showCustomerHistory() {
    var options = new Xrm.DialogOptions();
    options.height = top.innerHeight * 0.8;
    options.width = top.innerWidth * 0.6;
    if (options.width < 800) options.width = 800;
    var dialogUri = Mscrm.CrmUri.create("$webresource:lookers_customeractions.html").toString();
    dialogUri += "?id=" + encodeURIComponent(Xrm.Page.data.entity.getId()) + "&Data=" + encodeURIComponent(Xrm.Page.data.entity.getPrimaryAttributeValue());
    Xrm.Internal.openDialog(dialogUri, options);
}