var app = angular.module("CrmWebResourceApp", []).service("CrmWebResourceAppUtils",

function () {
    var xrmCtx = GetGlobalContext();
    var crmApiBaseUrl = xrmCtx.getClientUrl() + "/api/data/v8.2"

    // Set Common Headers
    var commonHeaders = {
        "Accept": "application/json",
        "OData-MaxVersion": "4.0",
        "OData-Version": "4.0",
        "If-None-Match": null
    }

    return {
        // Perform a Retrieve Multiple operation using CRM Web API
        retrieveMultiple: function ($http, entitySet, querySuffix, maxResults, onSuccess, onError) {
            // Set Headers
            var headers = commonHeaders;
            headers.Prefer = "odata.include-annotations=\"OData.Community.Display.V1.FormattedValue\"";

            // Add Header to limit results if required
            if (maxResults > 0) headers.Prefer += ",odata.maxpagesize=" + (maxResults + 1)

            // Perform HTTP GET
            $http({
                url: crmApiBaseUrl + "/" + entitySet + "?" + querySuffix,
                dataType: "json",
                method: "GET",
                headers: headers
            }).success(function (response) {
                // Call onSuccess callback
                var hasResults = (response.value.length > 0)
                var limitedToMax = (maxResults > 0 && response.value.length > maxResults);
                var results = (limitedToMax) ? response.value.slice(0, maxResults) : response.value;
                onSuccess(hasResults, results, limitedToMax);
            }).error(function (error) {
                // Call onError callback
                onError(error);
            });
        },

        // Perform a FetchXML Query using CRM Web API
        fetchXmlQuery: function ($http, entitySet, fetchXml, onSuccess, onError) {
            return this.retrieveMultiple($http, entitySet, "fetchXml=" + encodeURI(fetchXml), 0, onSuccess, onError);
        },

        // Create a record using CRM Web API
        create: function ($http, entitySet, attributes, onSuccess, onError) {
            // Perform HTTP POST
            $http({
                url: crmApiBaseUrl + "/" + entitySet,
                dataType: "json",
                method: "POST",
                headers: commonHeaders,
                data: JSON.stringify(attributes)
            }).success(function (response) {
                // Call onSuccess callback
                onSuccess(response);
            }).error(function (error) {
                // Call onError callback
                onError(error);
            });
        },

        // Retrieve a record using CRM Web API
        retrieve: function ($http, entitySet, id, querySuffix, onSuccess, onError) {
            // Perform HTTP GET
            $http({
                url: crmApiBaseUrl + "/" + entitySet + "(" + id + ")?" + querySuffix,
                dataType: "json",
                method: "GET",
                headers: commonHeaders,
            }).success(function (response) {
                // Call onSuccess callback
                onSuccess(response);
            }).error(function (error) {
                // Call onError callback
                onError(error);
            });
        },

        // Update a record using CRM Web API
        update: function ($http, entitySet, id, attributes, onSuccess, onError) {
            // Perform HTTP PATCH
            $http({
                url: crmApiBaseUrl + "/" + entitySet + "(" + id + ")",
                dataType: "json",
                method: "PATCH",
                headers: commonHeaders,
                data: JSON.stringify(attributes)
            }).success(function (response) {
                // Call onSuccess callback
                onSuccess(response);
            }).error(function (error) {
                // Call onError callback
                onError(error);
            });
        },

        // Delete a record using CRM Web API
        delete: function ($http, entitySet, id, onSuccess, onError) {
            // Perform HTTP POST
            $http({
                url: crmApiBaseUrl + "/" + entitySet + "(" + id + ")",
                dataType: "json",
                method: "DELETE",
                headers: commonHeaders,
            }).success(function (response) {
                // Call onSuccess callback
                onSuccess(response);
            }).error(function (error) {
                // Call onError callback
                onError(error);
            });
        },

        // Call an unbound action using CRM Web API
        unboundAction: function ($http, actionName, params, onSuccess, onError) {
            // Perform HTTP POST
            $http({
                url: crmApiBaseUrl + "/" + actionName,
                dataType: "json",
                method: "POST",
                headers: commonHeaders,
                data: JSON.stringify(params)
            }).success(function (response) {
                // Call onSuccess callback
                onSuccess(response);
            }).error(function (error) {
                // Call onError callback
                onError(error);
            });
        },

        // Return the parameters in the URL as a Javascript object
        getUrlParams: function () {
            var qs = decodeURIComponent(window.location.search.substring(1));
            var params = qs.split("&");
            var result = {};
            for (var i = 0; i < params.length; i++) {
                var param = params[i].split("=");
                result[param[0]] = param[1];
            }
            return result;
        },

        formatGuid: function (guid) {
            return guid.replace(/[^A-F\d\-]/ig, "").toLowerCase();
        }
    }
});