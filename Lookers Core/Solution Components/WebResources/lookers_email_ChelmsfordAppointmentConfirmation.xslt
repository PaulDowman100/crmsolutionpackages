<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE stylesheet [
  <!ENTITY nbsp  "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html" />
  <xsl:template match="entity">
    <font face="calibri" size="3">
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td align="right" valign="top">
            <img height="100" src="{f.lookers_logourl}" />
          </td>
        </tr>
      </table>
      Dear <xsl:value-of select="c.lookers_title" />&nbsp;<xsl:value-of select="c.lastname" />,
      <br />
      <br />Following our conversation, I am pleased to confirm your appointment for <xsl:value-of select="lookers_requestedappointmentdate" />.
      <br />
      <br />We look forward to seeing you at our Lookers <xsl:value-of select="d.lookers_displayname" /> Dealership at <xsl:value-of select="d.lookers_dealershipaddress" />.
      <br />
      <xsl:if test="starts-with(d.lookers_videourl, 'http')">
        <br /><em><a href="{d.lookers_videourl}">Find out more about Lookers <xsl:value-of select="d.lookers_displayname" /> on YouTube.</a></em>
        <br />
      </xsl:if>
      <br />Please ensure on the day of your visit, you bring your driving licence as well as your National Insurance Number with
      you to allow you to test drive our vehicles.
      <br />
      <br />In the meantime, if you would like further information on our current New and Used car offers, please visit our Lookers
      <xsl:value-of select="f.lookers_name" /> website, where you will also have access to over 10,000 Approved Used vehicles in stock.
      <br />
      <br />
      <a href="{f.lookers_franchisewebsite}">
        <xsl:value-of select="f.lookers_franchisewebsite" />
      </a>
      <br />
      <br />
      <strong>
        **We offer nationwide delivery and, subject to conditions, we can transfer cars between Lookers dealerships. There
        is a map of our dealership locations attached.**
      </strong>
      <br />
      <br />If there is anything else I can help with, please don’t hesitate to contact me on 01245 255352 or reply via email if
      you would prefer.
      <br />
      <br />I look forward to hearing from you soon.
      <br />
      <br />Kind regards,
      <br />
      <br />
      <strong>
        <span style="color: navy;">
          <xsl:value-of select="cu.fullname" />
          <br /><xsl:value-of select="cu.title" />
          <br />Business Development Centre
          <br />
          <span style="color: #999999;">Lookers PLC</span>
          <br />Tel: <xsl:value-of select="cu.address1_telephone1" />
          <br />Email:
          <a href="{cu.internalemailaddress}">
            <xsl:value-of select="cu.internalemailaddress" />
          </a>
          <br />Website:
          <a href="http://www.lookers.co.uk/">www.lookers.co.uk</a>
        </span>
      </strong>
      <br />
      <br />
      <img height="50" src="https://lksazcrmcorpstor01.blob.core.windows.net/logos/Lookers.jpg" />
      <img height="50" src="https://lksazcrmcorpstor01.blob.core.windows.net/logos/Keys.jpg" />
    </font>
  </xsl:template>
</xsl:stylesheet>